
from mylibrary import what_a_good_function

def test_good_function_does_not_return():
    assert what_a_good_function() is None
